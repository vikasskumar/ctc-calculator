import { Component, OnInit } from '@angular/core';
import { Salary } from './salary';

@Component({
  selector: 'app-salary',
  templateUrl: './salary.component.html',
  styleUrls: ['./salary.component.css']
})
export class SalaryComponent implements OnInit {

  salary: Salary = {
    basic: 0,
    bonus: 0,
    ca: 0,
    epfEmp: 0,
    epfEmpr: 0,
    esiEmp: 0,
    esiEmpr: 0,
    fa: 0,
    gpa: 0,
    gratuity: 0,
    hra: 0,
    lta: 0,
    medical: 0,
    montlyGross: 0,
    oa: 0,
    profesionalTax: 0,
    benefitsTotal: 0,
    deductionsTotal: 0,
    totalCtc: 0
  };
  showform: boolean;
  gross: number;
  salaryErrorMessage:string;
  constructor() { }

  ngOnInit() {
    this.showform = false;
  }

  search() {
    if (this.gross != undefined && this.gross < 224469) {
      this.salaryErrorMessage = 'CTC should be more than 224469'
     // this.calculateESI();
    }else if(this.gross != undefined && this.gross <= 295538) {
      this.showform = true;
      this.esiCTCtoGross();
    } else if (this.gross != undefined) {
      //this.calculateNewSlab();

      this.showform = true;
    } else {
      this.showform = false;
    }
  }
/*
    double ctc = 290000;
    	
    	double netpay = ctc*0.81;
    	double basic = 14100*12;
    	double bonus = 1175*12;
    	double hra = 0;
    	double epf = basic*0.12;
    	double grad = basic*0.0881;
    	double esi = netpay*0.0325;
    	
    	double total = netpay + epf+grad+esi;
    	double diff = ctc-total;
    	if(diff>0) {
    		hra = diff;
    	}
*/

  esiCTCtoGross() {
    this.gross;
  }

  initialiseAll() {
    this.salary.basic = 0;
    this.salary.bonus = 0;
    this.salary.ca = 0;
    this.salary.epfEmp = 0;
    this.salary.epfEmpr = 0;
    this.salary.esiEmp = 0;
    this.salary.esiEmpr = 0;
    this.salary.fa = 0;
    this.salary.gpa = 0;
    this.salary.gratuity = 0;
    this.salary.hra = 0;
    this.salary.lta = 0;
    this.salary.medical = 0;
    this.salary.montlyGross = 0;
    this.salary.oa = 0;
    this.salary.profesionalTax = 0;
    this.salary.benefitsTotal = 0;
    this.salary.deductionsTotal = 0;
    this.salary.totalCtc = 0;

  }

  calculateESI() {
    this.initialiseAll();
    this.salary.basic = 14100;
    this.salary.bonus = 1175;
    this.salary.hra = this.gross - this.salary.basic - this.salary.bonus;
    this.salary.montlyGross = this.salary.basic + this.salary.bonus + this.salary.hra;
    this.salary.epfEmp = this.salary.basic * 0.12;
    this.salary.profesionalTax = 200;
    this.salary.esiEmp = this.salary.basic * 0.0075;
    this.salary.epfEmpr = this.salary.basic * 0.12;
    this.salary.esiEmpr = this.gross * 0.0325;
    this.salary.deductionsTotal = this.salary.epfEmp + this.salary.profesionalTax + this.salary.esiEmp;
    this.salary.lta = 0;
    this.salary.medical = 0;
    this.salary.gpa = 0;
    this.salary.benefitsTotal = this.salary.epfEmpr + this.salary.gratuity + this.salary.esiEmpr + this.salary.gpa;
    this.salary.totalCtc = +((this.salary.montlyGross * 12) + (this.salary.benefitsTotal * 12)).toFixed(2)
  }

  calculateAboveSlab() {
    /*
        public basic:number,
            public hra:number,
            public ca:number,
            public lta:number,
            public bonus:number,
            public fa:number,
            public oa:number, 
            public montlyGross:number,
    
    */
    this.initialiseAll();
    this.salary.basic = this.gross * 0.40;
    if (this.gross < 26150 || this.salary.basic <15000) {
      this.salary.basic = 15000;
    }
    this.salary.hra = this.salary.basic * 0.40;
    this.salary.lta = this.salary.basic * 0.15;
    if (this.salary.basic > 21000) {
      this.salary.ca = 1600;
      this.salary.esiEmp = 0;
      this.salary.esiEmpr = 0;
    }
    if (this.salary.basic < 21000) {
      this.salary.bonus = 584;
    }
    this.salary.fa = 1300;
    this.salary.oa = this.gross - (this.salary.basic + this.salary.hra + this.salary.lta + this.salary.ca + this.salary.bonus + this.salary.fa);
    this.salary.montlyGross = this.salary.basic + this.salary.oa + this.salary.hra + this.salary.lta + this.salary.ca + this.salary.bonus + this.salary.fa;

    this.salary.epfEmp = this.salary.basic * 0.12;
    this.salary.profesionalTax = 200;
    this.salary.medical = 200;
    this.salary.deductionsTotal = this.salary.epfEmp + this.salary.profesionalTax + this.salary.esiEmp + this.salary.medical;

    this.salary.epfEmpr = this.salary.basic * 0.12;
    this.salary.gpa = 4500;
    this.salary.gratuity = this.salary.basic * 0.0881;
    this.salary.benefitsTotal = this.salary.epfEmpr + this.salary.gratuity + this.salary.esiEmpr + this.salary.gpa;
    this.salary.totalCtc = +((this.salary.montlyGross * 12) + (this.salary.benefitsTotal * 12)).toFixed(2)
  }


  calculateNewSlab() {
    let temp = 0;
    let remaining = 0;
    this.initialiseAll();
   // if (this.gross >= 21000 && this.gross <= 26150) {
    if (this.gross*0.40 <= 15000) {
      this.salary.basic = 15000;
    } else {
      this.salary.basic = this.gross * 0.40;
    }
    remaining = this.gross - this.salary.basic;
    temp = this.salary.basic * 0.40;
    if (remaining > temp) {
      this.salary.hra = temp;
      remaining = remaining - temp;
    } else {
      this.salary.hra = remaining;
    }
    if (remaining > 1600) {
      this.salary.ca = 1600;
      remaining = remaining - 1600;
    } else {
      this.salary.ca = remaining;
    }

    this.salary.epfEmpr = this.salary.basic * 0.12;
    this.salary.gpa = 4500;
    this.salary.gratuity = +((this.salary.basic * 12) * 0.0881).toFixed(2);
    this.salary.benefitsTotal = +((this.salary.epfEmpr*12) + this.salary.gratuity + this.salary.gpa).toFixed(2);
    this.salary.totalCtc = +((this.gross * 12) + this.salary.benefitsTotal).toFixed(2);

    this.salary.epfEmp = this.salary.basic * 0.12;
    this.salary.profesionalTax = 200;
    this.salary.medical = 200;
    this.salary.deductionsTotal = this.salary.epfEmp + this.salary.profesionalTax  + this.salary.medical;

    if(this.salary.totalCtc > 500000) {
        temp = this.salary.basic * 0.15;
        if(remaining > temp) {
          this.salary.lta = temp;
          remaining = remaining-temp;
        } else {
          this.salary.lta = remaining;
        }
    } 
    if(remaining > 1300) {
      this.salary.fa = 1300;
      remaining = remaining - 1300;
    } else {
      this.salary.fa = remaining;
    }
    if(remaining > 0) {
      this.salary.oa = remaining
    }
    this.salary.montlyGross = this.salary.basic + this.salary.oa + this.salary.hra + this.salary.lta + this.salary.ca + this.salary.bonus + this.salary.fa;

    //----------------------------------------------------------
    
    // this.salary.epfEmp = this.salary.basic * 0.12;
    // this.salary.profesionalTax = 200;
    // this.salary.medical = 200;
    // this.salary.deductionsTotal = this.salary.epfEmp + this.salary.profesionalTax + this.salary.medical;

    // this.salary.epfEmpr = this.salary.basic * 0.12;
    // this.salary.gpa = 4500;
    // this.salary.gratuity = this.salary.basic * 0.0881;
    // this.salary.benefitsTotal = this.salary.epfEmpr + this.salary.gratuity + this.salary.esiEmpr + this.salary.gpa;
    // this.salary.totalCtc = +((this.salary.montlyGross * 12) + (this.salary.benefitsTotal * 12)).toFixed(2)
  }

}
