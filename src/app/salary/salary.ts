export class Salary {
    constructor(
        public basic:number,
        public hra:number,
        public ca:number,
        public lta:number,
        public bonus:number,
        public fa:number,
        public oa:number, 
        public montlyGross:number,

        public epfEmp:number,
        public profesionalTax:number,
        public esiEmp:number,
        public medical:number,
        public deductionsTotal:number,

        public epfEmpr:number,
        public gratuity:number,
        public esiEmpr:number,
        public gpa:number,
        public benefitsTotal:number,

        public totalCtc:number
        ) {

    }
}